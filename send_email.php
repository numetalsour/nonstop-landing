<?php
    $nombre   = $_POST["nombre"];
    $email    = $_POST["email"];
    $telefono = $_POST["telefono"];
    $msg      = $_POST["msg"];

    if(strlen($nombre)<3){
        echo '<p style="color: red;">Su nombre debe contener al menos 4 caracteres.</p>';
    }else{
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            echo '<p style="color: red;">Su correo electrónico no es válido.</p>';
        }else{
            if(strlen($telefono) < 8){
                echo '<p style="color: red;">Su teléfono debe contener al menos 8 numeros.</p>';
            }else{
                if(strlen($msg) < 10){
                    echo '<p style="color: red;">Su mensaje debe contener al menos 10 caracteres.</p>';
                }else{

                    $message = '<br><br>
                    <p>Nombre        : '.$nombre.'</p>
                    <p>Email         : '.$email.'</p>
                    <p>Telefono      : '.$telefono.'</p>
                    <p>Mensaje       : '.$msg.'</p>';
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    mail("contacto@nonstop.cl","Formulario contacto", $message, $headers);

                    echo '<p style="color: green;">Su mensaje ha sido enviado exitosamente.</p>';
                }
            }

        }
    }
    
?>