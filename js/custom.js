$(document).ready(function () {

    //smooth scroll
    $(function () {
        $('.scroll').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 69
                    }, 800);
                    return false;
                }
            }
        });

        $('#btnMsg').click(function () {
            $("#status").html('<p style="color: #8f01bb;"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Enviando ...</p>');
            $.post("send_email.php", { 
                nombre: $('#nombre').val(),
                email: $('#email').val(),
                telefono:$('#telefono').val(),
                msg:$('#msg').val()
            }, function (result) {
                $("#status").html(result);
            });
        });
    });
});    